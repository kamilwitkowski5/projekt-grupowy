using System;
using UnityEngine;

namespace HunT.CommandLine {
    public delegate void ExecuteCommandHandler(params string[] arguments);
    public class Command {
        private int expectedArguments;
        ExecuteCommandHandler executeCommand;
        public Command(ExecuteCommandHandler executeCommand) {
            this.executeCommand = executeCommand;
        }

        public Command(int expectedArguments, ExecuteCommandHandler executeCommand) {
            this.expectedArguments = expectedArguments;
            this.executeCommand = executeCommand;
        }

        public void Execute(params string[] arguments) {
            if (arguments.Length == expectedArguments)
                executeCommand(arguments);
            else
                throw new WrongNumberOfArgumentsException($"Expected: {expectedArguments} found: {arguments.Length}");
        }

        class WrongNumberOfArgumentsException : Exception {
            public WrongNumberOfArgumentsException() {
                Debug.LogWarning("Incorrect number of arguments in command!");
            }
            public WrongNumberOfArgumentsException(string message) {
                Debug.LogWarning("Incorrect number of arguments in command!" + message);
            }
        }
    }
}