//------------------------------------------------------------------------------
// <auto-generated>
//     This code was auto-generated by com.unity.inputsystem:InputActionCodeGenerator
//     version 1.2.0
//     from Assets/Scripts/DebugCommandLine/DebugUIInput.inputactions
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public partial class @DebugUIKeymap : IInputActionCollection2, IDisposable
{
    public InputActionAsset asset { get; }
    public @DebugUIKeymap()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""DebugUIInput"",
    ""maps"": [
        {
            ""name"": ""DebugUI"",
            ""id"": ""ea8dee5e-e804-4c21-9b92-88ec92f1440f"",
            ""actions"": [
                {
                    ""name"": ""ToggleDebugUI"",
                    ""type"": ""Button"",
                    ""id"": ""47612e78-cca2-4491-9f70-92edd93260e5"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""SendCommand"",
                    ""type"": ""Button"",
                    ""id"": ""2fa32574-ea9c-41bf-b950-a7cb5bf7ef72"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""MemoryCommandUp"",
                    ""type"": ""Button"",
                    ""id"": ""01a80353-1dc3-4f22-88f2-0e9655ae77fd"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""MemoryCommandDown"",
                    ""type"": ""Button"",
                    ""id"": ""fb861489-758e-4420-8bf7-732f0f25de1c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""0bd49432-61f6-45ce-9bcc-0700ebcbefed"",
                    ""path"": ""<Keyboard>/#(`)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ToggleDebugUI"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ee906394-b69c-4876-b18c-d2397a11ef4b"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SendCommand"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""57614449-2300-4e8f-8357-ccb5df3ec3dd"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MemoryCommandUp"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7425c27c-de65-4330-a203-8186b78ee365"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MemoryCommandDown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // DebugUI
        m_DebugUI = asset.FindActionMap("DebugUI", throwIfNotFound: true);
        m_DebugUI_ToggleDebugUI = m_DebugUI.FindAction("ToggleDebugUI", throwIfNotFound: true);
        m_DebugUI_SendCommand = m_DebugUI.FindAction("SendCommand", throwIfNotFound: true);
        m_DebugUI_MemoryCommandUp = m_DebugUI.FindAction("MemoryCommandUp", throwIfNotFound: true);
        m_DebugUI_MemoryCommandDown = m_DebugUI.FindAction("MemoryCommandDown", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }
    public IEnumerable<InputBinding> bindings => asset.bindings;

    public InputAction FindAction(string actionNameOrId, bool throwIfNotFound = false)
    {
        return asset.FindAction(actionNameOrId, throwIfNotFound);
    }
    public int FindBinding(InputBinding bindingMask, out InputAction action)
    {
        return asset.FindBinding(bindingMask, out action);
    }

    // DebugUI
    private readonly InputActionMap m_DebugUI;
    private IDebugUIActions m_DebugUIActionsCallbackInterface;
    private readonly InputAction m_DebugUI_ToggleDebugUI;
    private readonly InputAction m_DebugUI_SendCommand;
    private readonly InputAction m_DebugUI_MemoryCommandUp;
    private readonly InputAction m_DebugUI_MemoryCommandDown;
    public struct DebugUIActions
    {
        private @DebugUIKeymap m_Wrapper;
        public DebugUIActions(@DebugUIKeymap wrapper) { m_Wrapper = wrapper; }
        public InputAction @ToggleDebugUI => m_Wrapper.m_DebugUI_ToggleDebugUI;
        public InputAction @SendCommand => m_Wrapper.m_DebugUI_SendCommand;
        public InputAction @MemoryCommandUp => m_Wrapper.m_DebugUI_MemoryCommandUp;
        public InputAction @MemoryCommandDown => m_Wrapper.m_DebugUI_MemoryCommandDown;
        public InputActionMap Get() { return m_Wrapper.m_DebugUI; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(DebugUIActions set) { return set.Get(); }
        public void SetCallbacks(IDebugUIActions instance)
        {
            if (m_Wrapper.m_DebugUIActionsCallbackInterface != null)
            {
                @ToggleDebugUI.started -= m_Wrapper.m_DebugUIActionsCallbackInterface.OnToggleDebugUI;
                @ToggleDebugUI.performed -= m_Wrapper.m_DebugUIActionsCallbackInterface.OnToggleDebugUI;
                @ToggleDebugUI.canceled -= m_Wrapper.m_DebugUIActionsCallbackInterface.OnToggleDebugUI;
                @SendCommand.started -= m_Wrapper.m_DebugUIActionsCallbackInterface.OnSendCommand;
                @SendCommand.performed -= m_Wrapper.m_DebugUIActionsCallbackInterface.OnSendCommand;
                @SendCommand.canceled -= m_Wrapper.m_DebugUIActionsCallbackInterface.OnSendCommand;
                @MemoryCommandUp.started -= m_Wrapper.m_DebugUIActionsCallbackInterface.OnMemoryCommandUp;
                @MemoryCommandUp.performed -= m_Wrapper.m_DebugUIActionsCallbackInterface.OnMemoryCommandUp;
                @MemoryCommandUp.canceled -= m_Wrapper.m_DebugUIActionsCallbackInterface.OnMemoryCommandUp;
                @MemoryCommandDown.started -= m_Wrapper.m_DebugUIActionsCallbackInterface.OnMemoryCommandDown;
                @MemoryCommandDown.performed -= m_Wrapper.m_DebugUIActionsCallbackInterface.OnMemoryCommandDown;
                @MemoryCommandDown.canceled -= m_Wrapper.m_DebugUIActionsCallbackInterface.OnMemoryCommandDown;
            }
            m_Wrapper.m_DebugUIActionsCallbackInterface = instance;
            if (instance != null)
            {
                @ToggleDebugUI.started += instance.OnToggleDebugUI;
                @ToggleDebugUI.performed += instance.OnToggleDebugUI;
                @ToggleDebugUI.canceled += instance.OnToggleDebugUI;
                @SendCommand.started += instance.OnSendCommand;
                @SendCommand.performed += instance.OnSendCommand;
                @SendCommand.canceled += instance.OnSendCommand;
                @MemoryCommandUp.started += instance.OnMemoryCommandUp;
                @MemoryCommandUp.performed += instance.OnMemoryCommandUp;
                @MemoryCommandUp.canceled += instance.OnMemoryCommandUp;
                @MemoryCommandDown.started += instance.OnMemoryCommandDown;
                @MemoryCommandDown.performed += instance.OnMemoryCommandDown;
                @MemoryCommandDown.canceled += instance.OnMemoryCommandDown;
            }
        }
    }
    public DebugUIActions @DebugUI => new DebugUIActions(this);
    public interface IDebugUIActions
    {
        void OnToggleDebugUI(InputAction.CallbackContext context);
        void OnSendCommand(InputAction.CallbackContext context);
        void OnMemoryCommandUp(InputAction.CallbackContext context);
        void OnMemoryCommandDown(InputAction.CallbackContext context);
    }
}
