using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class swordPrototype : MonoBehaviour
{
    Animator _animator;
    

    PlayerInputActions _controls;

    bool _isPerformingStrongAttack;

    [Header("Ustawienia Ataku")]
    public float fastAttackSpeed;
    public float strongAttackSpeed;
    public float blockSpeed ;
    public float swordIdleSpeed;



    private void Awake()
    {
        _controls = new PlayerInputActions();
        _controls.Enable();

    }
    private void Update()
    {
        _animator.SetFloat("IdleSpeed", swordIdleSpeed);
    }
    private void Start()
    {
        _animator = GetComponent<Animator>();

        _controls.OnFoot.FastAttack.performed += ctx => Attacking(true);    
        _controls.OnFoot.Block.performed += ctx => ToggleBlock(true);
        _controls.OnFoot.Block.canceled += ctx => ToggleBlock(false);
        _controls.OnFoot.StrongAttack.performed += ctx => _isPerformingStrongAttack=true;
        _controls.OnFoot.StrongAttack.canceled += ctx => _isPerformingStrongAttack = false;
    }

    void Attacking(bool fastAttack)
    {
        if (fastAttack && !_isPerformingStrongAttack)
        {
            FastAttack();
        }
        else if (fastAttack&&_isPerformingStrongAttack)
        {
            StrongAttack();
        }
    }
    void FastAttack()
    {
        _animator.SetFloat("FastAttackSpeed", fastAttackSpeed);
        _animator.SetTrigger("FastAttack");
    }
    void StrongAttack()
    {
        _animator.SetFloat("StrongAttackSpeed", strongAttackSpeed);
        _animator.SetTrigger("StrongAttack");
    }
    void ToggleBlock(bool activ)
    {
        _animator.SetFloat("BlockSpeed", blockSpeed);
        if (activ)
        {
            _animator.SetBool("Block", true);
        }
        else
        {
            _animator.SetBool("Block", false);

        }
    }
}
